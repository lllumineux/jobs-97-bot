"""
This module sets logging, connects to DB, creates bot object and calls function
that initializes all bot command processing methods
"""

import logging

from discord.ext import commands
from mongoengine import connect

from jobs_97_bot import config
from jobs_97_bot.config import COMMAND_PREFIX
from jobs_97_bot.utils.add_all_cogs import add_all_cogs

logging.basicConfig(level=logging.DEBUG)

connect(host=config.DB_LINK)

bot = commands.Bot(command_prefix=COMMAND_PREFIX, help_command=None)

add_all_cogs(bot)
