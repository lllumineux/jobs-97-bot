"""
This module stores project's config variables
"""

import os

from dotenv import load_dotenv

load_dotenv(".env")

# Discord config
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")
DISCORD_CLIENT_ID = os.getenv("DISCORD_CLIENT_ID")
DISCORD_PERMISSIONS = os.getenv("DISCORD_PERMISSIONS")
DISCORD_SCOPE = os.getenv("DISCORD_SCOPE")

# MongoDB config
DB_NAME = os.getenv("DB_NAME")
DB_USER_NAME = os.getenv("DB_USER_NAME")
DB_USER_PASSWORD = os.getenv("DB_USER_PASSWORD")
DB_LINK = (
    f"mongodb+srv://{DB_USER_NAME}:{DB_USER_PASSWORD}@cluster0.zer4f"
    f".mongodb.net/{DB_NAME}?retryWrites=true&w=majority"
)

# Project config
APP_FOLDER_NAME = "jobs_97_bot"
IMG_FOLDER_NAME = "img"
CARD_COMBINATIONS_FOLDER_NAME = "card_combinations"

# Bot config
CATEGORY_NAME = "jobs-97"
TEXT_CHANNEL_NAME = "game-process"
VOICE_CHANNEL_NAME = "discussion-room"
COMMAND_PREFIX = "!"
EMBED_MESSAGE_COLOR = 0x000000

# Session config (basics)
MIN_PLAYER_AMOUNT = 5
MAX_PLAYER_AMOUNT = 99

# Session config (states)
VP_CEO_CHOICE_STATE = "vp-candidate-choice"
CEO_CANDIDATE_VOTING_STATE = "ceo-candidate-voting"
VP_CARD_CHOICE = "vp-card-choice"
CEO_CARD_CHOICE = "ceo-card-choice"
