"""
This module provides useful functions that can be used in any of the other
project files

List of functions:

get_embed - returns embed to attach to the message
send_dm - sends direct message to the list of players
get_id_from_discord_mention - returns player_id from discord mention
get_index - returns card index or None if provided arg is not correct
"""

import os

import discord
from discord.ext.commands import Bot

from jobs_97_bot.config import (
    APP_FOLDER_NAME,
    CARD_COMBINATIONS_FOLDER_NAME,
    EMBED_MESSAGE_COLOR,
    IMG_FOLDER_NAME,
)
from jobs_97_bot.db.db_utils import (
    get_opponents_card_amount,
    get_supporters_card_amount,
)


def get_embed(
    title: str = "",
    description: str = "",
    footer: str = "",
    img_name: str = "",
    paragraphs: list = None,
    server_id: int = 0,
):
    embed = discord.Embed(colour=EMBED_MESSAGE_COLOR)

    if title:
        embed.title = title
    if description:
        embed.description = description
    if footer:
        embed.set_footer(text=footer)
    if img_name:
        embed.set_image(url=f"attachment://{img_name}")
    if paragraphs:
        for paragraph in paragraphs:
            embed.add_field(
                name=paragraph["title"],
                value=paragraph["text"],
                inline=False,
            )
    if server_id:
        supporters_card_count = get_supporters_card_amount(server_id)
        opponents_card_count = get_opponents_card_amount(server_id)
        if supporters_card_count > opponents_card_count:
            text = (
                f"Счёт - {supporters_card_count}:{opponents_card_count} "
                f"в пользу сторонников"
            )
        elif opponents_card_count > supporters_card_count:
            text = (
                f"Счёт - {opponents_card_count}:{supporters_card_count} "
                f"в пользу противников"
            )
        else:
            text = f"Счёт - {opponents_card_count}:{supporters_card_count}"
        embed.set_author(name=text)

    return embed


async def send_dm(
    bot: Bot,
    players_id_list: list,
    text: str = "",
    title: str = "",
    description: str = "",
    footer: str = "",
    img_name: str = "",
):
    for player_id in players_id_list:
        try:
            player = bot.get_user(player_id)
            if text:
                await player.send(text)
            else:
                if img_name:
                    file = discord.File(
                        os.path.join(
                            os.getcwd(),
                            APP_FOLDER_NAME,
                            IMG_FOLDER_NAME,
                            CARD_COMBINATIONS_FOLDER_NAME,
                            f"{img_name}.png",
                        ),
                        filename=f"{img_name}.png",
                    )
                    await player.send(
                        file=file,
                        embed=get_embed(
                            title=title,
                            description=description,
                            footer=footer,
                            img_name=f"{img_name}.png",
                        ),
                    )
                else:
                    await player.send(
                        embed=get_embed(
                            title=title,
                            description=description,
                            footer=footer,
                        )
                    )

        except AttributeError:
            pass


def get_id_from_discord_mention(mention):
    try:
        return int(mention[3:-1:] if "!" in mention else mention[2:-1:])
    except ValueError:
        return None


def get_index(index):
    try:
        return int(index) if 1 <= int(index) <= 3 else None
    except ValueError:
        return None
