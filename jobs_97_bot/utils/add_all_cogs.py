"""
This module initializes bot command processing functions
"""

from jobs_97_bot.cogs.general import General
from jobs_97_bot.cogs.round_processing import RoundProcessing
from jobs_97_bot.cogs.session_management import SessionManagement


def add_all_cogs(bot):
    bot.add_cog(General(bot))
    bot.add_cog(SessionManagement(bot))
    bot.add_cog(RoundProcessing(bot))
